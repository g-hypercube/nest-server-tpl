import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { MemberService } from 'src/member/member.service';
import { Users } from './users.entity';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { Member } from 'src/member/member.entity';
@Controller('users')
export class UsersController {
  constructor(
    private usersService: UsersService,
    private memberService: MemberService,
  ) {}
  @Get('init_superman')
  async init(): Promise<Users> {
    return this.usersService.initSuperMan();
  }

  @UseGuards(AuthGuard())
  @Get('list')
  async list(@Req() req: Request): Promise<Users[]> {
    console.log(req.user);
    return this.usersService.getList();
  }
  @UseGuards(AuthGuard())
  @Get('info')
  async getInfo(@Req() req: any): Promise<any> {
    const user_id = req.user.id;
    const user = await this.usersService.findOne(user_id);
    const member_id = user.member_id;
    const member = await this.memberService.info(member_id);
    return {
      ...member,
    };
  }
}
