import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from './users.entity';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private userRepository: Repository<Users>,
  ) {}

  async findOneByName(name) {
    return this.userRepository.findOne({ where: { account: name } });
  }
  async getList() {
    return this.userRepository.find();
  }
  async findOne(id) {
    return this.userRepository.findOne({ where: { id: id } });
  }
  /**
   * 初始化初始用户
   * @returns
   */
  async initSuperMan(): Promise<Users> {
    let user: Users = await this.userRepository.findOne({
      where: {
        account: 'admin',
      },
    });
    if (user) {
      return user;
    } else {
      user = await this.userRepository.save({
        account: 'admin',
        password: 'admin',
      });
      return user;
    }
  }
}
