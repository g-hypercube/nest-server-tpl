import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AllExceptionsFilter } from './middleware/any-exception.filters';
import { HttpRequestMiddleware } from './middleware/request.middleware';
import { HttpResponseIntercepter } from './middleware/response.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(new HttpRequestMiddleware().use);
  app.useGlobalInterceptors(new HttpResponseIntercepter());
  app.useGlobalFilters(new AllExceptionsFilter());
  await app.listen(3000);
}
bootstrap();
