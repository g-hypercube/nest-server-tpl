import * as Log4js from 'log4js';
import { Logger } from 'src/utils/log4js';
const logger = Log4js.getLogger('http');
export class HttpLogger implements Logger {
  static error(...args) {
    logger.error(Logger.getStackTrace(), ...args);
  }
  static access(...args) {
    logger.info(Logger.getStackTrace(), ...args);
  }
}
