import { Controller, Post, Body, Inject } from '@nestjs/common';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('/login')
  async login(@Body() loginParams: { account: string; password: string }) {
    console.log(loginParams);
    const authResult = await this.authService.validateUser(
      loginParams.account,
      loginParams.password,
    );
    console.log(authResult.user);
    switch (authResult.code) {
      case 1:
        const token = await this.authService.certificate(authResult.user);
        return {
          code: 200,
          token: token,
        };
      case 2:
        return {
          code: 600,
          msg: `账号或密码不正确`,
        };
      case 3:
        return {
          code: 600,
          msg: `账号不存在`,
        };
    }
  }
}
