import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { MemberModule } from './member/member.module';
import { AuthModule } from './auth/auth.module';
import { AppController } from './app.controller';
import { environment } from 'config';

@Module({
  imports: [
    TypeOrmModule.forRoot(environment),
    UsersModule,
    MemberModule,
    AuthModule,
  ],
  providers: [],
  controllers: [AppController],
})
export class AppModule {}
