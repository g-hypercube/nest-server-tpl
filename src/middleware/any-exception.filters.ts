import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { HttpLogger } from 'src/logger/logger';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    const request = host.switchToHttp().getRequest();
    const response = host.switchToHttp().getResponse();
    const status =
      exception instanceof HttpException ? exception.getStatus() : 500;
    // 自定义的异常信息结构
    const error_info = exception.response ? exception.response : exception;
    const error_data = exception.response?.data ? exception.response.data : {};
    const error_msg = exception.response
      ? exception.response.message
        ? exception.response.message
        : exception.response.errorMsg
      : 'server internal error';

    const error_code = exception.response?.errorCode
      ? exception.response.errorCode
      : 500;

    // 自定义异常结构体， 日志用
    const data = {
      timestamp: new Date().toISOString(),
      ip: request.ip,
      reqUrl: request.originalUrl,
      reqMethod: request.method,
      httpCode: status,
      params: request.params,
      query: request.query,
      body: request.body,
      statusCode: error_code,
      errorMsg: error_msg,
      errorData: error_data,
      errorInfo: error_info,
    };
    if (status === HttpStatus.NOT_FOUND) {
      data.errorMsg = `资源不存在！接口${request.method} -> ${request.url} 无效！`;
    }
    HttpLogger.error(data);
    response.status(status).json({
      data: data.errorData,
      msg: data.errorMsg,
      code: data.statusCode,
    });
  }
}
