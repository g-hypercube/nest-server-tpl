import * as path from 'path';
const baseLogPath = path.resolve(__dirname, '../logs');

const log4jsConfig = {
  appenders: {
    console: { type: 'console' }, // 打印到控制台
    access: {
      type: 'dateFile', // 写入文件格式，并按照日期分类
      filename: `${baseLogPath}/access/access.log`,
      alwaysIncludePattern: true,
      pattern: 'yyyy-MM-dd',
      //maxLogSize: 10485760,
      daysToKeep: 30,
      numBackups: 30,
      compress: true,
      category: 'http',
      keepFileExt: true,
    },
    app: {
      type: 'dateFile', // 写入文件格式，并按照日期分类
      filename: `${baseLogPath}/app-out/app.log`,
      alwaysIncludePattern: true,
      layout: {
        type: 'pattern',
        pattern:
          "[%d{yyyy-MM-dd hh:mm:ss SSS}] [%p] -h: %h -pid: %z msg: '%m' ",
      },
      pattern: 'yyyy-MM-dd',
      //maxLogSize: 10485760,
      daysToKeep: 30,
      numBackups: 30,
      keepFileExt: true,
    },
    errorFile: {
      type: 'dateFile',
      filename: `${baseLogPath}/error/error.log`,
      alwaysIncludePattern: true,
      layout: {
        type: 'pattern',
        pattern:
          "[%d{yyyy-MM-dd hh:mm:ss SSS}] [%p] -h: %h -pid: %z msg: '%m' ",
      },
      pattern: 'yyyy-MM-dd',
      //maxLogSize: 10485760,
      daysToKeep: 30,
      numBackups: 30,
      keepFileExt: true,
    },
    errors: {
      type: 'logLevelFilter',
      level: 'ERROR',
      appender: 'errorFile',
    },
  },
  categories: {
    default: {
      appenders: ['console', 'access', 'app', 'errors'],
      level: 'DEBUG',
    },
    mysql: { appenders: ['access', 'errors'], level: 'info' },
    http: { appenders: ['access'], level: 'DEBUG' },
  },
};
export default log4jsConfig;
