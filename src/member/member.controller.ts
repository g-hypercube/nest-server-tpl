import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Member } from './member.entity';
import { MemberService } from './member.service';
import { Request } from 'express';
@Controller('member')
export class MemberController {
  constructor(private memberService: MemberService) {}

  @UseGuards(AuthGuard())
  @Get('list')
  async getList(@Req() request: Request): Promise<Member[]> {
    const params: any = {};
    if (request.query.team_id) {
      params.team_id = parseInt(request.query.team_id as string);
    } else {
      return Promise.reject(`参数不全${request.query}`);
    }
    if (request.query.isParty !== undefined && request.query.isParty !== null) {
      params.isParty = parseInt(request.query.isParty as string);
    }
    if (request.query.roleName) {
      params.roleName = request.query.roleName;
    }
    return await this.memberService.getList(params);
  }

  @UseGuards(AuthGuard())
  @Post('add')
  async add(
    @Req() req: Request,
    @Body() body: Partial<Member>,
  ): Promise<Member> {
    console.log(req.authInfo, req.user);
    return this.memberService.add(body);
  }

  @UseGuards(AuthGuard())
  @Patch(':id')
  async edit(
    @Param('id') id: number,
    @Body() body: Partial<Member>,
  ): Promise<any> {
    return this.memberService.update(id, body);
  }

  @UseGuards(AuthGuard())
  @Delete(':id')
  async delet(@Param('id') id: number): Promise<any> {
    return this.memberService.delete(id);
  }

  @UseGuards(AuthGuard())
  @Get('info')
  async getInfoByParams(@Req() request: Request) {
    return this.memberService.searchOne(request.query);
  }

  @UseGuards(AuthGuard())
  @Get(':id/info')
  async info(@Param('id') id: number): Promise<Member> {
    return this.memberService.info(id);
  }
}
