import { RoleName } from 'src/const';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn({ type: 'int' })
  id: number;

  @Column({ comment: '姓名', nullable: false })
  name: string;

  @Column({ comment: '出生年月' })
  birth: string;

  @Column({
    comment: 'role',
    type: 'enum',
    enum: RoleName,
    default: RoleName.Normal,
  })
  roleName: RoleName;

  @Column({ comment: '简介', type: 'longtext' })
  intro: string;

  @Column({ comment: '小组Id', nullable: true })
  team_id: number;

  @Column({ comment: '照片' })
  photo: string;

  @Column({ comment: '是否党员', type: 'int', default: 0 })
  isParty: number;
}
