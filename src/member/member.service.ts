import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Member } from './member.entity';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member) private memberRepository: Repository<Member>,
  ) {}

  getList(params: { team_id: number; isParty?: number }): Promise<Member[]> {
    if (params.team_id) {
      return this.memberRepository.find({ where: params });
    } else {
      return Promise.reject(`参数不全；${params}`);
    }
  }

  add(member: Partial<Member>): Promise<Member> {
    return this.memberRepository.save(member);
  }

  update(id: number, member: Partial<Member>): Promise<UpdateResult> {
    return this.memberRepository.update(id, member);
  }

  delete(id: number): Promise<DeleteResult> {
    return this.memberRepository.delete(id);
  }
  info(id: number): Promise<Member> {
    return this.memberRepository.findOne({ where: { id } });
  }
  searchOne(params: Partial<Member>) {
    return this.memberRepository.findOne({ where: params });
  }
}
