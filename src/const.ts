export enum RoleName {
  PartyLeader = 'PartyLeader', // 党小组组长
  ManageLeader = 'ManageLeader', // 管理班组长
  WorkLeader = 'WorkLeader', // 工作班组长
  Whistleblower = 'Whistleblower', //吹哨人
  Normal = 'Normal', //成员
}
