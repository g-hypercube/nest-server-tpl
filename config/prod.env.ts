import { DataSourceOptions } from 'typeorm';
import { DbLogger } from 'src/utils/log4js';

const db: DataSourceOptions = {
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'gyx_123!',
  database: 'nest-tpl',
  entities: ['dist/**/*.entity{.ts,.js}'],
  synchronize: true,
  logging: true,
  maxQueryExecutionTime: 1000,
  logger: new DbLogger(),
  timezone: '+08:00',
};
export default db;
