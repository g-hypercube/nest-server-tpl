FROM node:14.20.0 AS production

WORKDIR /opt/nest

COPY . .
RUN npm config set registry https://mirrors.huaweicloud.com/repository/npm/
RUN npm install;
ENV NODE_ENV=production
CMD ["node", "./dist/main.js"]
